#![allow(unused)]

use serde_yaml;

use std::fs;
use std::io::{self, Write};
use std::path::PathBuf;

const DEFAULT_CONFIG: &str = include_str!("default_config.yaml");

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    // bind_addrs: Vec<String>, // Ignored for now, will implement this later.
    listen_port: u16,
    max_db_size: String, // In bytes, can use formats like "100M"
    db_cache_cap: String,
}

#[derive(Debug)]
pub enum ConfigError {
    IoError(io::Error),
    ParseError(serde_yaml::Error),
    InvalidDbSize
}

impl From<io::Error> for ConfigError {
    fn from(f: io::Error) -> Self {
        ConfigError::IoError(f)
    }
}

impl From<serde_yaml::Error> for ConfigError {
    fn from(f: serde_yaml::Error) -> Self {
        ConfigError::ParseError(f)
    }
}

impl Config {
    pub fn load_or_create_with_with_path(p: PathBuf) -> Result<Config, ConfigError> {

        // First, just load it.
        let cfg: Config = {
            if p.exists() {
                // Just read the config from file.
                let mut f = fs::File::open(p)?;
                serde_yaml::from_reader(&mut f)?
            } else {

                // Write the default config.
                let mut f = fs::File::create(p)?;
                f.write_all(String::from(DEFAULT_CONFIG).as_ref())?;

                // Then continue from the same thing we just wrote.
                serde_yaml::from_str(DEFAULT_CONFIG)?
            }
        };

        // Assert some invariants.
        if parse_byte_size(&cfg.max_db_size).is_err() {
            return Err(ConfigError::InvalidDbSize)
        }

        if parse_byte_size(&cfg.db_cache_cap).is_err() {
            return Err(ConfigError::InvalidDbSize)
        }

        // Looks good, return it.
        Ok(cfg)

    }

    pub fn default_with_port(port: u16) -> Config {
        let mut c = Self::default();
        c.listen_port = port;
        c
    }

    pub fn listen_port(&self) -> u16 {
        self.listen_port
    }

    pub fn max_db_size_bytes(&self) -> u64 {
        parse_byte_size(&self.max_db_size).unwrap() // should have been verified beforehand, unwrapping should be ok
    }

    pub fn db_cache_cap(&self) -> u64 {
        parse_byte_size(&self.db_cache_cap).unwrap()
    }
}

impl Default for Config {
    fn default() -> Self {
        serde_yaml::from_str(DEFAULT_CONFIG).unwrap() // this should probably work
    }
}

fn parse_byte_size(v: &String) -> Result<u64, &'static str> {
    if v.len() <= 1 {
        return Err("string not long enough")
    }

    match v.clone().parse() {
        Ok(v) => Ok(v),
        Err(_) => {
            let mult = match v.chars().last().unwrap() {
                'B' => 1,
                'K' => 1 << 10,
                'M' => 1 << 20,
                'G' => 1 << 30,
                'T' => 1 << 40,
                _ => return Err("unknown suffix")
            };
            match String::from(&v[..(v.len() - 1)]).parse::<u64>() {
                Ok(v) => Ok(mult * v),
                Err(_) => Err("prefix not a number")
            }
        }
    }
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn test_default_config_parse() {
        let _ = Config::default();
    }

}
