extern crate qpcore;

extern crate jsonrpc_core;
extern crate jsonrpc_minihttp_server;

#[macro_use]
extern crate clap;

extern crate serde;
extern crate serde_yaml;

#[macro_use]
extern crate serde_derive;

extern crate sled;

use std::env;
use std::path::PathBuf;
use std::sync::*;

use jsonrpc_minihttp_server::cors::AccessControlAllowOrigin;
use jsonrpc_minihttp_server::jsonrpc_core::*;
use jsonrpc_minihttp_server::*;

use qpcore::p2p::Rpc;

mod config;
mod node;
mod rpcops;

use config::Config;

fn main() {
    // Parse program arguments.
    let matches = clap_app!(qpserver =>
        (version: "0.1.0")
        (author: "treyzania <treyzania@gmail.com>")
        (about: "Quickpixel swarm node.")
        (@arg datadir: --datadir -d +takes_value "Data directory to store application data, default is cwd")
        (@arg config: --config -c +takes_value "Config file path, will create if not found")
        (@arg port: -p +takes_value "Listen port, ignored if config provided")
    ).get_matches();

    // Extract some values we care about.
    let data_dir_path: Option<String> = matches.value_of("datadir").map(String::from);
    let cfg_path: Option<String> = matches.value_of("config").map(String::from);
    let port_arg: Option<u16> = match matches.value_of("port").map(str::parse) {
        Some(Ok(p)) => Some(p),
        Some(Err(_)) => {
            println!("port supplied is invalid");
            return;
        }
        None => None,
    };

    // Read the config from file, do some fancy business to figure things out.
    println!("Loading config...");
    let cfg = match cfg_path {
        Some(p) => match Config::load_or_create_with_with_path(PathBuf::from(p)) {
            Ok(c) => c,
            Err(e) => {
                println!("config parse error: {:?}", e);
                return;
            }
        },
        None => match port_arg {
            Some(p) => Config::default_with_port(p),
            None => Config::default(),
        },
    };

    // Figure out where to put data, if unspecified then just put it here.
    let data_dir = match data_dir_path {
        Some(d) => PathBuf::from(d),
        None => match env::current_dir() {
            Ok(p) => p,
            Err(e) => {
                println!("data dir init error: {:?}", e);
                return;
            }
        },
    };

    // Actually create the node.
    println!("Initializing node...");
    let node = match node::QpNode::new(&cfg, &data_dir) {
        Ok(n) => Arc::new(n),
        Err(e) => {
            println!("node init error: {:?}", e);
            return;
        }
    };
    println!("Node inited!");

    // Set up JSON-RPC call system.
    let mut io = IoHandler::new();
    let rpc = rpcops::RpcImpl::new(node.clone());
    io.extend_with(rpc.to_delegate());

    // Start the server with the RPC config.
    let server = ServerBuilder::new(io)
        .cors(DomainsValidation::AllowOnly(vec![
            AccessControlAllowOrigin::Null,
        ]))
        .start_http(&format!("127.0.0.1:{}", cfg.listen_port()).parse().unwrap())
        .map_err(|e| println!("error initing RPC: {:?}", e))
        .unwrap();

    // Actually start it.
    server.wait().unwrap();
}
