#![allow(unused)]

use jsonrpc_core::{Error, ErrorCode, Result};

use serde_json;

use qpcore::crypto::*;
use qpcore::messaging;
use qpcore::p2p;
use qpcore::user;

use sled;

use node;

pub struct RpcImpl {
    node: node::NodeRef,
}

impl RpcImpl {
    pub fn new(n: node::NodeRef) -> RpcImpl {
        RpcImpl { node: n }
    }
}

impl p2p::Rpc for RpcImpl {
    fn announce_user_def(&self, udef: user::UserDefinition) -> Result<bool> {
        if user::check_udef_pow(&udef)
            .map_err(|e| Error::invalid_params(format!("work error: {:?}", e)))?
        {
            // Serialize it before we acquire the lock.
            let udef_json = serde_json::to_vec(&udef).map_err(|e| Error::internal_error())?;

            // Pick out the database.
            let db: &sled::Tree = &self.node.user_db;

            // Add it to the DB and log it.
            db.set(Vec::from(udef.name.as_bytes()), udef_json);
            println!("adding new user: {}", udef.name);

            // TODO Queue to send def to peers.
            return Ok(true);
        } else {
            // TODO Make this more consistent.
            println!("user announcement failed PoW check: {}", udef.name);
            return Err(Error::new(ErrorCode::ServerError(1000)));
        }
    }

    fn get_user_names(&self) -> Result<Vec<String>> {
        // Set up where we're writing to.
        let mut names_raw: Vec<Vec<u8>> = Vec::new();

        // Pick out the database.
        let db: &sled::Tree = &self.node.user_db;

        // Scan over the DB keys.
        for r in db.scan(&*vec![]) {
            match r {
                Ok((k, _)) => {
                    names_raw.push(k);
                }
                Err(e) => {
                    println!("db scan error: {:?}", e);
                }
            }
        }

        // Convert them to strings as UTF-8.
        let names: Vec<String> = names_raw
            .into_iter()
            .map(String::from_utf8)
            .filter_map(|v| match v {
                Ok(n) => Some(n),
                Err(e) => {
                    println!("error parsing name in DB: {:?}", e);
                    None
                }
            })
            .collect();

        Ok(names)
    }

    fn get_user_def(&self, name: String) -> Result<user::UserDefinition> {
        /*
        if name == "bob" {
                    return Ok(user::UserDefinition::new_unchecked(
                        "bob".into(),
                        "".into(),
                        "".into(),
                        [0; 32],
                        EcPrivkey::new_rand().unwrap().into_pubkey().unwrap(),
                    ));
                }
        */

        // Get the actual raw user data from the DB.
        let raw = (self.node.user_db)
            .get(name.as_bytes())
            .map_err(|e| Error::internal_error())?;

        // If not found, return that.
        if raw.is_none() {
            return Err(Error::invalid_params("user not found"));
        }

        // Parse it and return the error if we messed up.
        let udef = serde_json::from_slice(raw.unwrap().as_slice()).map_err(|e| {
            println!("error parsing user def for user {}: {:?}", name, e);
            Error::internal_error()
        })?;

        // Should all be good here now.
        Ok(udef)
    }

    fn publish_message(&self, msg: messaging::Message) -> Result<bool> {
        Ok(true)
    }

    fn claim_message(&self, preimage: Box<[u8]>) -> Result<messaging::Message> {
        Err(Error::new(ErrorCode::ServerError(42)))
    }

    fn request_pin(&self, addr: String) -> Result<bool> {
        Err(Error::new(ErrorCode::ServerError(42)))
    }

    fn get_swarm(&self) -> Result<Vec<p2p::ServerAddr>> {
        Err(Error::new(ErrorCode::ServerError(42)))
    }
}
