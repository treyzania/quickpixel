#![allow(unused)]

use std::fs;
use std::io;
use std::path::PathBuf;
use std::sync::{mpsc, Arc, Mutex, MutexGuard};

use sled;

use qpcore::user;

use config;

pub struct QpNode {
    pub user_db: sled::Tree,
    pub content_db: sled::Tree,
    pub pin_db: sled::Tree,
    //announcement_queue: mpsc::Sender<PeerAnnouncement>,
}

pub type NodeRef = Arc<QpNode>;

pub enum PeerAnnouncement {
    UserDef(user::UserDefinition),
}

#[derive(Debug)]
pub enum NodeInitError {
    ConfigError,
    IoError(io::Error),
    DbError(sled::Error<()>),
    NetError,
}

impl From<io::Error> for NodeInitError {
    fn from(f: io::Error) -> NodeInitError {
        NodeInitError::IoError(f)
    }
}

impl From<sled::Error<()>> for NodeInitError {
    fn from(f: sled::Error<()>) -> NodeInitError {
        NodeInitError::DbError(f)
    }
}

impl QpNode {
    pub fn new(cfg: &config::Config, datadir: &PathBuf) -> Result<QpNode, NodeInitError> {
        if !datadir.exists() {
            fs::create_dir(datadir)?;
        }

        let udb = sled::Tree::start(create_sled_config(cfg, datadir, "users"))?;
        let cdb = sled::Tree::start(create_sled_config(cfg, datadir, "content"))?;
        let pdb = sled::Tree::start(create_sled_config(cfg, datadir, "pin"))?;

        Ok(QpNode {
            user_db: udb,
            content_db: cdb,
            pin_db: pdb,
        })
    }

    pub fn shutdown(self) {
        // just consume itself
    }
}

fn create_sled_config(rootcfg: &config::Config, datadir: &PathBuf, db: &str) -> sled::Config {
    sled::ConfigBuilder::new()
        .path({
            let mut pb = datadir.clone();
            pb.push(db);
            pb
        })
        .cache_capacity(rootcfg.db_cache_cap() as usize)
        .use_compression(true)
        .flush_every_ms(Some(5000)) // 5 second flush period
        .snapshot_after_ops(100000)
        .build()
}
