#!/usr/bin/env python3

import sys
import secrets
import json

import rpc

c = rpc.QpRpc('http://localhost:32010/')

def make_random_array(len):
	pkarr = []
	for _ in range(len):
		pkarr.append(int(secrets.randbits(8)))
	return pkarr

name = sys.argv[1]
addr = 'NYI' # TODO
chan = 'NYI' # TODO
nonce = make_random_array(32) # TODO
pubkey = make_random_array(32) # TODO

udef = {
	'name': name,
	'ipns_addr': addr,
	'ipfs_pubsub_channel': chan,
	'nonce': nonce,
	'pubkey': pubkey
}

print(json.dumps(udef))

print('Pushing...')
res = c.announceuser(udef)

print(json.dumps(res, indent=2))
