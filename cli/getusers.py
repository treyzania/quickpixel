#!/usr/bin/env python3
#!/usr/bin/env python3

import sys
import json

import rpc

c = rpc.QpRpc('http://localhost:32010/')

if len(sys.argv) >= 2:
	name = sys.argv[1]
	try:
		res = c.getuserbyname(name)
		print(json.dumps(res, indent=2))
	except Exception as e:
		print(str(e))
else:
	res = c.getuserindex()
	print(json.dumps(res, indent=2))
