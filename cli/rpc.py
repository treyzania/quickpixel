import json
import requests

class QpRpc():
	def __init__(self, url):
		self.url = url

	def call(self, name, args):
		req = {
			'method': name,
			'params': args,
			'jsonrpc': '2.0',
			'id': 1 # TODO Make this better later.
		}
		resp = requests.post(self.url, json=req)
		if resp.status_code == 200:
			rj = resp.json()
			if 'result' in rj:
				return rj['result']
			else:
				raise AssertionError(json.dumps(rj['error']))
		else:
			raise AssertionError('bad request code: ' + str(resp.status_code))

	def __getattr__(self, name):
		def delegate(*args):
			return self.call(name, args)
		return delegate
