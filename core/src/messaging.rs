
use crypto::*;

use openssl::sha;

#[derive(Clone, Hash, Serialize, Deserialize)]
pub struct Announcement {
    // Should this be signed by the recipient when being accepted?  Then servers would need to know
    // the recipient's pubkey and we might not want those (or a hashes) to be global.
    revocation_preimage: Box<[u8]>,
    data_hash: [u8; 32], // also serves as the IV for decryption of the message
    aes_key: AesKey
}

/// This is the actual message stored as-is on the server.  The payload is encrypted so the server
/// can't see the contents.  And it is expected to delete it when someone provides the preimage to
/// the hash.
#[derive(Clone, Hash, Serialize, Deserialize)]
pub struct Message {
    revocation_hash: [u8; 32],
    data: Vec<u8>,
}

#[derive(Debug)]
pub enum MessageCreateError {
    Crypt(CryptoError),
    UnexpectedOverflow
}

impl From<CryptoError> for MessageCreateError {
    fn from(f: CryptoError) -> MessageCreateError {
        MessageCreateError::Crypt(f)
    }
}

pub fn create_message(msg: &[u8], rev_preimage: &[u8], aes_key: &AesKey) -> Result<(Announcement, Message), MessageCreateError> {

    // First we hash the input to figure out the initialization vector.
    let msghash = sha::sha256(msg);

    // Now actually do the encryption.
    let buf = aes_key.encrypt_with_iv(msg, &sha256_hash_to_aes_cbc_iv(&msghash))?;

    // Build the announcement.
    let ann = Announcement {
        revocation_preimage: Vec::from(rev_preimage).into_boxed_slice(),
        data_hash: msghash,
        aes_key: aes_key.clone()
    };

    // Now build the revocation hash.
    let hash = sha::sha256(rev_preimage);

    // Build the outgoing message.
    let omsg = Message {
        revocation_hash: hash,
        data: buf
    };

    // Now return the final tuple.
    Ok((ann, omsg))

}

pub enum MessageExtractError {
    KeyFail
}

pub fn extract_message(ann: Announcement, msg: Message) -> Result<Vec<u8>, MessageCreateError> {

    /*
     * Most of this is just the inverse of the encryption function.
     */

    // Pull out the AES IV which is just the hash of the data.
    let iv = sha256_hash_to_aes_cbc_iv(&ann.data_hash);

    // Now just simply decrypt it.
    let buf = ann.aes_key.decrypt(msg.data.as_ref(), &iv)?;

    // Now return the actual buffer we read from.
    Ok(buf)
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn test_enc_dec_1mib() {

        let mut msg = vec![0; 1 << 20];
        let pre = [42u8; 64];
        let key = AesKey::new([1u8; 32]);

        // Now put some deterministic random data in it.
        for i in 0..msg.len() {
            msg[i] = 7u64.pow(i as u32 % 19) as u8 // just some primes to make it interesting
        }

        match create_message(msg.as_ref(), &pre, &key) {
            Ok((a, m)) => match extract_message(a, m) {
                Ok(v) => {
                    // Actually verify the message is correct.
                    for i in 0..(1 << 20) {
                        assert_eq!(msg[i], v[i]);
                    }
                },
                Err(e) => {
                    println!("{:?}", e);
                    assert!(false);
                }
            },
            Err(e) => {
                println!("{:?}", e);
                assert!(false);
            }
        }

    }

}
