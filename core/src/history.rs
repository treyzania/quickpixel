
use user;

// Actual user state information, refs to the history chain.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct UserState {
    id_table_key: [u8; 32],
    active_user: user::User,
    current_page: String,
    first_page: String,
}

/// A chunk of history, also with some user key information.
#[derive(Clone, Hash, Debug, Serialize, Deserialize)]
pub struct HistoryPage {
    prev_page: Option<String>,
    user_info: user::User,
    entries: Vec<HistoryEntry> // ordered, although shouldn't matter
}

/// Stores information about some action that the user did at some time.
#[derive(Clone, Hash, Debug, Serialize, Deserialize)]
pub struct HistoryEntry {
    timestamp: u64,
    act: Action
}

/// This info for the actual "thing" that the user did.
#[derive(Clone, Hash, Debug, Serialize, Deserialize)]
pub enum Action {
    Comment(String)
    // TODO Add more
}
