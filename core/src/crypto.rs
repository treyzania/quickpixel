/// This module contains a bunch of primitives for doing cryptographic operations.  It stores data
/// at rest in DER-encoded byte vectors.

use openssl::bn::BigNumContext;
use openssl::ec::*;
use openssl::ecdsa;
use openssl::error::ErrorStack;
use openssl::pkey;
use openssl::nid::Nid;
use openssl::rsa::*;
use openssl::sha;
use openssl::symm::{Cipher, decrypt, encrypt};

#[derive(Clone, Eq, PartialEq, Hash, Debug, Serialize, Deserialize)]
pub struct RsaPrivkey(Vec<u8>);

impl RsaPrivkey {
    pub fn new_rand() -> Result<RsaPrivkey, CryptoError> {
        let sk: Rsa<pkey::Private> = Rsa::generate(256)?;
        Ok(RsaPrivkey(sk.private_key_to_der()?))
    }
    pub fn into_pubkey(&self) -> Result<RsaPubkey, CryptoError> {
        let sk: Rsa<pkey::Private> = Rsa::private_key_from_der(&self.0)?;
        Ok(RsaPubkey(sk.public_key_to_der()?))
    }
}

#[derive(Clone, Eq, PartialEq, Hash, Debug, Serialize, Deserialize)]
pub struct RsaPubkey(Vec<u8>);

#[derive(Clone, Eq, PartialEq, Hash, Debug, Serialize, Deserialize)]
pub struct EcPrivkey(Vec<u8>);

impl EcPrivkey {
    pub fn new_rand() -> Result<EcPrivkey, CryptoError> {
        let sk: EcKey<pkey::Private> = EcKey::generate(get_curve_group().as_ref())?;
        Ok(EcPrivkey(sk.private_key_to_der()?))
    }
    pub fn into_pubkey(&self) -> Result<EcPubkey, CryptoError> {

        // Set up env because curves are complicated.
        let curve = get_curve_group();
        let mut bnctx = BigNumContext::new()?;

        // Reconstruct things.
        let sk: EcKey<pkey::Private> = EcKey::private_key_from_der(&self.0)?;

        // Then actually serializing the pubkey is trivial.
        let der = sk.public_key().to_bytes(&curve, PointConversionForm::COMPRESSED, &mut bnctx)?;

        Ok(EcPubkey(der))
    }
    pub fn sign_msg(&self, msg: &[u8]) -> Result<EcSig, CryptoError> {

        // Reconstruct privkey.
        let sk: EcKey<pkey::Private> = EcKey::private_key_from_der(&self.0)?;

        // Hash and sign.
        let hash = sha::sha256(msg);
        let sig = ecdsa::EcdsaSig::sign(&hash, &sk)?;

        Ok(EcSig(sig.to_der()?))
    }
}

#[derive(Clone, Eq, PartialEq, Hash, Debug, Serialize, Deserialize)]
pub struct EcPubkey(Vec<u8>);

impl EcPubkey {
    pub fn verify_sig(&self, msg: &[u8], sig: &EcSig) -> Result<bool, CryptoError> {

        // Setup.
        let curve = get_curve_group();
        let mut bnctx = BigNumContext::new()?;

        // Decode things.
        let point = EcPoint::from_bytes(&curve, &self.0, &mut bnctx)?;
        let pk: EcKey<pkey::Public> = EcKey::from_public_key(&curve, &point)?;
        let nsig = ecdsa::EcdsaSig::from_der(&sig.0)?;

        // Hash it again.
        let hash = sha::sha256(msg);

        // Actually verify
        Ok(nsig.verify(&hash, &pk)?)
    }

    pub fn inner(&self) -> &Vec<u8> {
        &self.0
    }

    pub fn from_vec(v: Vec<u8>) -> EcPubkey {
        EcPubkey(v) // TODO Verification?
    }
}

#[derive(Clone, Eq, PartialEq, Hash, Debug, Serialize, Deserialize)]
pub struct EcSig(Vec<u8>);

/// 256-bit AES CBC key
#[derive(Clone, Eq, PartialEq, Hash, Debug, Serialize, Deserialize)]
pub struct AesKey([u8; 32]);

impl AesKey {
    pub fn new(v: [u8; 32]) -> AesKey {
        AesKey(v)
    }

    /// Encrypts the message deterministically, using the SHA-256 hash as the AES IV.
    pub fn encrypt_iv_hash(&self, msg: &[u8]) -> Result<Vec<u8>, CryptoError> {
        let iv = sha256_hash_to_aes_cbc_iv(&sha::sha256(msg));
        self.encrypt_with_iv(msg, &iv)
    }

    /// Encrypts the data with the specified IV.
    pub fn encrypt_with_iv(&self, msg: &[u8], iv: &[u8; 16]) -> Result<Vec<u8>, CryptoError> {
        Ok(encrypt(Cipher::aes_256_cbc(), &self.0[..], Some(iv), msg)?)
    }

    /// Decrypts the data with the specified IV.
    pub fn decrypt(&self, msg: &[u8], iv: &[u8; 16]) -> Result<Vec<u8>, CryptoError> {
        Ok(decrypt(Cipher::aes_256_cbc(), &self.0, Some(iv), msg)?)
    }

    /// Returns self as bytes array.
    pub fn inner(&self) -> &[u8; 32] {
        &self.0
    }
}

/// This just compresses the hash into 16 bytes from 32 bytes.  Maybe we should just truncate?
#[inline]
pub fn sha256_hash_to_aes_cbc_iv(hash: &[u8; 32]) -> [u8; 16] {
    let mut iv = [0u8; 16];
    for i in 0..16 {
        iv[i] = hash[i] ^ hash[i + 16];
    }
    iv
}

#[derive(Debug)]
pub enum CryptoError {
    SslError(String),
    OtherStuff
}

impl From<ErrorStack> for CryptoError {
    fn from(f: ErrorStack) -> Self {
        CryptoError::SslError(format!("{:?}", f))
    }
}

// Curve is used in Bitcoin and a lot of other cryptocurrencies use it because it's really neat.
const CURVE: Nid = Nid::SECP256K1;

fn get_curve_group() -> EcGroup {
    EcGroup::from_curve_name(CURVE).unwrap()
}

#[cfg(test)]
pub mod test {

    use super::*;

    #[test]
    fn test_ec_sig_ok() {

        let msg = vec![42; 1024];
        let sk = EcPrivkey::new_rand().unwrap();
        let pk = sk.into_pubkey().unwrap();

        let sig = sk.sign_msg(msg.as_ref()).unwrap();
        assert!(pk.verify_sig(msg.as_ref(), &sig).unwrap());

    }

    #[test]
    fn test_ec_sig_fail() {

        let msg = vec![42; 1024];
        let sk = EcPrivkey::new_rand().unwrap();
        let bad = EcPrivkey::new_rand().unwrap().into_pubkey().unwrap();

        let sig = sk.sign_msg(msg.as_ref()).unwrap();
        assert!(!bad.verify_sig(msg.as_ref(), &sig).unwrap()); // notice the '!'

    }

}
