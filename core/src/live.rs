
use serde_json;

use history;
use user;
use crypto;

/// Messages coming in from pubsub users channels.  The signture can be validated with the pubkey
/// from the userdef associated with the channel.
#[derive(Clone, Hash, Debug, Serialize, Deserialize)]
pub struct LiveMessage {
    sig: crypto::EcSig,
    data: Vec<u8> // serialization doesn't need to be perfectly deterministic
}

#[derive(Debug)]
pub enum LiveMessageError {
    DataDecodeError,
    SigVerifcationFailed,
    CryptoError(crypto::CryptoError)
}

impl From<serde_json::Error> for LiveMessageError {
    fn from(_: serde_json::Error) -> LiveMessageError {
        LiveMessageError::DataDecodeError
    }
}

impl From<crypto::CryptoError> for LiveMessageError {
    fn from(f: crypto::CryptoError) -> LiveMessageError {
        LiveMessageError::CryptoError(f)
    }
}

impl LiveMessage {
    pub fn into_hist_entry(&self) -> Result<history::HistoryEntry, LiveMessageError> {
        Ok(serde_json::from_slice(self.data.as_slice())?)
    }
}

pub fn create_live_msg(id: user::Identity, e: &history::HistoryEntry) -> Result<LiveMessage, LiveMessageError> {
    let j = serde_json::to_vec(e).unwrap(); // TODO Make sure this is actually safe.
    let s = id.sign_message(j.as_ref())?;
    Ok(LiveMessage {
        sig: s,
        data: j
    })
}
