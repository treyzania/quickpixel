
use history;
use user;

pub enum SwarmError {
    ItemNotFound,
    NetworkError(String),
}

/// Wrapper for how history iterators should act.
pub trait HistoryIterator: Iterator<Item=(user::User, history::HistoryEntry)> {
    // nothing
}

/// Implementation of something letting a server connect to the swarm.
pub trait SwarmHook {

    /// Submits user data to the swarm, as a new user.
    fn submit_user(&mut self, def: user::UserDefinition, first_page: history::HistoryPage) -> Result<(String, history::UserState), SwarmError>;

    /// Tries to find a user by name, if applicable.
    fn find_user_def(&mut self, name: String) -> Result<user::UserDefinition, SwarmError>;

    /// Returns an iterator over the user's history.
    fn history_iter<I: HistoryIterator>(&mut self, name: String) -> Result<I, SwarmError>;

    /// Updates references, appending a history page onto the tip of the list, and broadcasting new entries.
    fn append_history(&mut self, u: user::User, items: &Vec<history::HistoryEntry>) -> Result<String, SwarmError>;

    // TODO Iterator for actions from pubsub channel.

}
