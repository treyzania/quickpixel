use jsonrpc_core::Result;

use messaging::Message;
use user::UserDefinition;

#[derive(Clone, Hash, Serialize, Deserialize)]
pub enum ServerAddr {
    Ipv4(String, u16),
}

build_rpc_trait! {
    pub trait Rpc {

        /// Announces a user definition, gossiping it to peers.
        #[rpc(name = "announceuser")]
        fn announce_user_def(&self, UserDefinition) -> Result<bool>;

        /// Gets a list of all the names this node knows about.
        #[rpc(name = "getuserindex")]
        fn get_user_names(&self) -> Result<Vec<String>>;

        /// Gets the full definition for a name.
        #[rpc(name = "getuserbyname")]
        fn get_user_def(&self, String) -> Result<UserDefinition>;

        /// Publishes a message to be stored on this node.
        #[rpc(name = "publishmessage")]
        fn publish_message(&self, Message) -> Result<bool>;

        /// Claims a message using the revocation preimage.
        #[rpc(name = "claimmessage")]
        fn claim_message(&self, Box<[u8]>) -> Result<Message>;

        /// Requests this node pin an IPFS object.
        #[rpc(name = "requestpin")]
        fn request_pin(&self, String) -> Result<bool>;

        /// Returns all of the peers this node knows about, including disconnected ones.
        #[rpc(name = "getswarm")]
        fn get_swarm(&self) -> Result<Vec<ServerAddr>>;
    }
}
