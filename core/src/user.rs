use std::io;
use std::string;

use byteorder::{ReadBytesExt, WriteBytesExt};

use openssl::sha;

use crypto::*;

#[derive(Clone, Eq, PartialEq, Hash, Debug, Serialize, Deserialize)]
pub struct User {
    signing_pubkey: EcPubkey,
    inbox_pubkey: RsaPubkey,
}

#[derive(Clone, Hash, Debug, Serialize, Deserialize)]
pub struct Identity {
    signing_privkey: EcPrivkey,
    inbox_privkey: RsaPrivkey,
}

impl Identity {
    pub fn new_rand() -> Result<Identity, CryptoError> {
        Ok(Identity {
            signing_privkey: EcPrivkey::new_rand()?,
            inbox_privkey: RsaPrivkey::new_rand()?,
        })
    }
    pub fn into_user(&self) -> Result<User, CryptoError> {
        Ok(User {
            signing_pubkey: self.signing_privkey.into_pubkey()?,
            inbox_pubkey: self.inbox_privkey.into_pubkey()?,
        })
    }
    pub fn sign_message(&self, msg: &[u8]) -> Result<EcSig, CryptoError> {
        Ok(self.signing_privkey.sign_msg(msg)?)
    }
}

#[derive(Debug)]
pub enum UserError {
    NameTooLong,
    AddrTooLong,
    ChannelTooLong,
    PubkeyTooLong,
}

#[derive(Debug)]
pub enum UserIoError {
    Data(UserError),
    Io(io::Error),
    Utf8(string::FromUtf8Error),
    UnexpectedEOF,
}

impl From<io::Error> for UserIoError {
    fn from(from: io::Error) -> Self {
        UserIoError::Io(from)
    }
}

impl From<string::FromUtf8Error> for UserIoError {
    fn from(from: string::FromUtf8Error) -> Self {
        UserIoError::Utf8(from)
    }
}

/// UserDefinition is the link between a username and the IPFS definitions.  This needs very
/// deterministic serialization because its hash is important for the DHT.
#[derive(Clone, Hash, Debug, Serialize, Deserialize)]
pub struct UserDefinition {
    pub name: String,

    pub ipns_addr: String,
    pub ipfs_pubsub_channel: String,

    /// We make short names harder to accept so the nonce is used for extra randomness.
    nonce: [u8; 32], // 32 bytes of randomness should be enough, can also just use a new pubkey if you need more.  also this should be serialized first

    /// Used to sign the initial state and then verify signatures of updated states.
    pub pubkey: EcPubkey,
}

impl UserDefinition {
    pub fn new_unchecked(
        name: String,
        addr: String,
        chan: String,
        nonce: [u8; 32],
        pk: EcPubkey,
    ) -> UserDefinition {
        UserDefinition {
            name: name,
            ipns_addr: addr,
            ipfs_pubsub_channel: chan,
            nonce: nonce,
            pubkey: pk,
        }
    }

    pub fn to_bytes<W: WriteBytesExt>(&self, w: &mut W) -> Result<(), UserIoError> {
        // Write the nonce first because of how hasing works.
        w.write(&self.nonce)?;

        if self.name.len() > 255 {
            return Err(UserIoError::Data(UserError::NameTooLong));
        }

        if self.ipns_addr.len() > 255 {
            return Err(UserIoError::Data(UserError::AddrTooLong));
        }

        if self.ipfs_pubsub_channel.len() > 255 {
            return Err(UserIoError::Data(UserError::ChannelTooLong));
        }

        if self.pubkey.inner().len() > 255 {
            return Err(UserIoError::Data(UserError::PubkeyTooLong));
        }

        // Write the name.
        w.write_u8(self.name.len() as u8)?;
        w.write(self.name.as_bytes())?;

        // Write the IPNS addr.
        w.write_u8(self.ipns_addr.len() as u8)?;
        w.write(self.ipns_addr.as_bytes())?;

        // Write the channel name.
        w.write_u8(self.ipfs_pubsub_channel.len() as u8)?;
        w.write(self.ipfs_pubsub_channel.as_bytes())?;

        // Write the pubkey.
        w.write_u8(self.pubkey.inner().len() as u8)?;
        w.write(self.pubkey.inner().as_ref())?;

        Ok(())
    }

    pub fn from_bytes<R: ReadBytesExt>(r: &mut R) -> Result<Self, UserIoError> {
        // Read the nonce.
        let mut nonce = [0u8; 32];
        r.read_exact(&mut nonce)?;

        // Read the name.
        let namelen = r.read_u8()? as usize;
        let mut readbuf = vec![0; namelen];
        r.read_exact(readbuf.as_mut())?;
        let name = String::from_utf8(readbuf)?;

        // Read the IPFS addr.
        let addrlen = r.read_u8()? as usize;
        let mut addrbuf = vec![0; addrlen];
        r.read_exact(addrbuf.as_mut())?;
        let addr = String::from_utf8(addrbuf)?;

        // Read te channel name.
        let chanlen = r.read_u8()? as usize;
        let mut chanbuf = vec![0; chanlen];
        r.read_exact(chanbuf.as_mut())?;
        let chan = String::from_utf8(chanbuf)?;

        // Read the pubkey.
        let pklen = r.read_u8()? as usize;
        let mut pk = vec![0; pklen];
        r.read_exact(pk.as_mut())?;

        Ok(UserDefinition {
            name: name,

            ipns_addr: addr,
            ipfs_pubsub_channel: chan,

            nonce: nonce,
            pubkey: EcPubkey::from_vec(pk),
        })
    }
}

// Based on the length of the string, returns how many 0 bits should be at the beginning of the
// string to satisfy the PoW
fn compute_pow_difficulty(_len: usize) -> usize {
    0 // TODO Make this harder.
}

// Returns the number of 0 bits at the beginning of a string.
fn buf_zero_bits(buf: &[u8]) -> usize {
    let mut bits = 0;
    for i in 0..buf.len() {
        let b = buf[i];
        for j in 0..8 {
            let s = (b >> (7 - j)) & 0x01;
            if s == 1 {
                break;
            } else {
                bits += 1
            }
        }
    }

    bits
}

/// Verifies if the UserDefinition satisfies the rarity constrants.
pub fn check_udef_pow(udef: &UserDefinition) -> Result<bool, UserIoError> {
    let mut writer = io::BufWriter::new(Vec::new());
    udef.to_bytes(&mut writer)?;
    let buf = writer.into_inner().unwrap(); // probably always works since it's in-memory

    let hash = sha::sha256(buf.as_ref());
    Ok(buf_zero_bits(&hash) >= compute_pow_difficulty(udef.name.len()))
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_zero_bits_1() {
        assert_eq!(0, buf_zero_bits(&[0x80]));
    }
    #[test]
    fn test_zero_bits_2() {
        assert_eq!(8, buf_zero_bits(&[0, 0xf0]));
    }
    #[test]
    fn test_zero_bits_3() {
        assert_eq!(3, buf_zero_bits(&[0x10]));
    }
    #[test]
    fn test_zero_bits_4() {
        assert_eq!(20, buf_zero_bits(&[0x00, 0x00, 0x0f, 0xbc]));
    }
    #[test]
    fn test_zero_bits_5() {
        assert_eq!(4, buf_zero_bits(&[0x08, 0xca, 0xfe, 0xba, 0xbe]));
    }
    #[test]
    fn test_zero_bits_6() {
        assert_eq!(0, buf_zero_bits(&[0xde, 0xad, 0xbe, 0xef]));
    }
}
